//
//  StatView.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/3/23.
//

import SwiftUI

struct StatisticsBarView: View {
    let statistic: Statistic

    var body: some View {
        VStack (alignment: .leading){
            Text(statistic.category)
           HStack {
                Rectangle()
                    .frame(width: CGFloat(statistic.value) * 10, height: 20)
                    .foregroundColor(Color.blue)
                    .padding()
                Text("\(Int(statistic.value))")
                    .foregroundColor(.black)
                   
            }
        }
        .padding()
    }
}

struct StatView: View {
    var peopleList:[DukePerson] = Array(sharedModel.database.values)
    var body: some View {
        let csStudents: [DukePerson] = peopleList.filter { person in
            // Check if the description contains "Computer Science" or "CS" keywords
            let keywords = ["Computer Science", "CS"]
            for keyword in keywords {
                if person.description.localizedCaseInsensitiveContains(keyword) {
                    return true
                }
            }
            return false
        }
        let eceStudents: [DukePerson] = peopleList.filter { person in
            // Check if the description contains "Computer Science" or "CS" keywords
            let keywords = ["ECE", "Egr Elec", "Electrical & Computer Egr", "Engineering"]
            for keyword in keywords {
                if person.description.localizedCaseInsensitiveContains(keyword) {
                    return true
                }
            }
            return false
        }
        
        let Others: [DukePerson] = peopleList.filter { person in
            // Check if the description contains "Computer Science" or "CS" keywords
            if !csStudents.contains(person) && !eceStudents.contains(person) {
                return true
            }
            return false
        }
        
        let statistics: [Statistic] = [
            Statistic(category: "CS Students", value: Double(csStudents.count)),
            Statistic(category: "ECE Students", value: Double(eceStudents.count)),
            Statistic(category: "Others", value: Double(Others.count)),
        ]
        
        List(statistics, id: \.category) { statistic in
                    StatisticsBarView(statistic: statistic)
                }
        .padding()
    }
}
