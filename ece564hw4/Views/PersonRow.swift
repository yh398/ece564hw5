//
//  MapView.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import SwiftUI
import UIKit

struct PersonRow: View {
    var person: DukePerson

    var body: some View {
        let image: Image = Image(uiImage: person.picture.imageFromBase64!)
        HStack {
            CircleImage(image: image)
                .frame(width: 150, height: 150)
            Spacer()
            VStack (alignment: .leading){
                Text("\(person.fName) \(person.lName)")
                    .font(.headline)
                Text("\(person.email)")
                    .font(.footnote)
                Text("DUID: \(String(person.DUID))")
                    .font(.footnote)
                Text("NetID: \(person.netid)")
                    .font(.footnote)
                Text("\(person.program)")
                    .font(.footnote)
                Text("\(person.plan)")
                    .font(.footnote)
            }
            Spacer()
        }
        .padding()
    }
}

//#Preview {
//    PersonRow(person: myInfo)
//}
