//
//  PersonDetail.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import SwiftUI

struct PersonDetail: View {
    var person: DukePerson
    var body: some View {
        ScrollView {
            CircleImage(image: Image(uiImage: person.picture.imageFromBase64!))
                .offset(y: 50)
                .padding(.bottom, -130)
                .frame(width: 200, height: 200)
            
            VStack(alignment: .leading) {
                Text("\(person.fName) \(person.lName)")
                    .font(.title)

                Divider()


                Text("About \(person.fName) \(person.lName)")
                    .font(.title2)
                Text(person.description)
            }
            .offset(y:150)
            .padding()

        }
        .navigationTitle("\(person.fName) \(person.lName)")
        .navigationBarTitleDisplayMode(.inline)
    }
}

//#Preview {
//    PersonDetail(person: myInfo)
//}
