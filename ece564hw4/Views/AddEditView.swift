//
//  AddEditView.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/5/23.
//

import SwiftUI

struct AddEditView: View {
    
    @EnvironmentObject var model: DataModel
    
    @Binding var isEditing: Bool
    @State var person: DukePerson
    @State private var gender: Gender
    @State private var role: Role
    @State private var from: String
    @State private var languages: [String]
    @State private var hobby: String
    @State private var program: String
    @State private var plan: String
    @State private var movie: String
    @State private var netid: String
    @State private var team: String
    @State private var languageInput: String
    @State private var showButton: Bool
    @State private var sendToServer = false
    
    @State private var selectedImage: Image?
    @State private var isImagePickerPresented = false
    
    
    init(isPresented: Binding<Bool>, person: DukePerson) {
        self._isEditing = isPresented
        self.person = person
        self.gender = person.gender
        self.role = person.role
        self.from = person.from
        self.languages = person.languages
        self.hobby = person.hobby
        self.program = person.program
        self.plan = person.plan
        self.movie = person.movie
        self.netid = person.netid
        self.team = person.team
        self.languageInput = person.languages.joined(separator: ", ")
        if person.DUID == myInfo.DUID {
            self.showButton = true
        } else {
            self.showButton = false
        }
    }
   
    let genderOptions = [.Male, .Female, Gender.Other]
    let roleOptions = [.Professor, .Student, .TA, Role.Other]
    
    var body: some View {
        ScrollView {
            CircleImage(image: Image(uiImage: person.picture.imageFromBase64!))
                .offset(y: -50)
                .padding(.bottom, -130)
                .frame(width: 150, height: 150)
            Spacer()
            VStack (spacing:10){
                TextField("First Name", text: $person.fName)
                TextField("Last Name", text: $person.lName)
            }
            .padding()
            
//            VStack {
//                   if selectedImage != nil {
//                       selectedImage!
//                           .resizable()
//                           .aspectRatio(contentMode: .fit)
//                           .frame(width: 200, height: 200)
//                           .onTapGesture {
//                               isImagePickerPresented.toggle()
//                           }
//                   } else {
//                       Button("Add Photo") {
//                           isImagePickerPresented.toggle()
//                       }
//                   }
//               }
//               .sheet(isPresented: $isImagePickerPresented) {
//                   ImagePicker(selectedImage: $selectedImage)
//               }
//            Spacer()
            
            if self.showButton {
                Toggle(isOn: $sendToServer) {
                   Text("Send to server")
                       .background(Color.clear) // Clear background
                }
                .padding(.horizontal, 100) // Add horizontal padding to the toggle
                .padding(.leading, 20)
                .background(Color.clear)
                Spacer()
            }
            
            Button("Add / Update"){
                person.gender = self.gender
                person.role = self.role
                person.from = self.from
                person.languages = self.languages
                person.hobby = self.hobby
                person.program = self.program
                person.plan = self.plan
                person.movie = self.movie
                person.netid = self.netid
                person.team = self.team
                if person.DUID == 0 {
                    person.DUID = uuidToInt(UUID())
                }
                person.languages = self.languageInput.split(separator: ",").map { $0.trimmingCharacters(in: .whitespaces) }
                if sendToServer {
                    _ = sharedModel.upload(website: server, auth: auth, update: true) {success, error in
                        if success {
                            print("Upload success")
                        } else {
                            if let error = error {
                                print("Upload failed with error: \(error.localizedDescription)")
                            } else {
                                print("Failed with an unknown error")
                            }
                        }
                    }

                }
                _ = sharedModel.update(person)
                _ = sharedModel.save()
                isEditing = false
            }
            
            Spacer()
           
            VStack(alignment: .center, spacing: 10) {
                HStack {
                    Text("Gender:")
                    Spacer()
                    Picker("Select an option", selection: $gender) {
                                    ForEach(genderOptions, id: \.self) { option in
                                        Text(option.rawValue)
                                    }
                                }
                                .pickerStyle(SegmentedPickerStyle())
                                .frame(width: 290, height: 40)
                }
                
                HStack {
                    Text("Role: ")
                    Spacer()
                    Picker("Select an option", selection: $role) {
                            ForEach(roleOptions, id: \.self) { option in
                                Text(option.rawValue)
                            }
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        .frame(width: 290, height: 40)
                }
                
                HStack {
                    Text("From: ")
                    Spacer()
                    TextField("Enter your location", text: $from)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }

                HStack {
                    Text("Language: ")
                    Spacer()
                    TextField("Enter programming languages", text: $languageInput)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
               
                HStack {
                    Text("Hobby: ")
                    Spacer()
                    TextField("Enter your hobby", text: $hobby)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
                
                HStack {
                    Text("Program: ")
                    Spacer()
                    TextField("Enter your program", text: $person.program)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
                
                HStack {
                    Text("Plan: ")
                    Spacer()
                    TextField("Enter your plan", text: $plan)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
               
                HStack {
                    Text("Movie: ")
                    Spacer()
                    TextField("Enter your favorite movie", text: $movie)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
                
                HStack {
                    Text("NetID: ")
                    Spacer()
                    TextField("Enter your NetID", text: $person.netid)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
                
                HStack {
                    Text("Team: ")
                    Spacer()
                    TextField("Enter your team", text: $team)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(width: 250, height: 40)
                }
                
            }
            .padding()
            
        }
    }
}

struct ImagePicker: UIViewControllerRepresentable {
    @Binding var selectedImage: Image?

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = context.coordinator
        return imagePicker
    }

    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}

    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        var parent: ImagePicker

        init(_ imagePicker: ImagePicker) {
            parent = imagePicker
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
            if let uiImage = info[.originalImage] as? UIImage {
                parent.selectedImage = Image(uiImage: uiImage)
            }
            picker.dismiss(animated: true, completion: nil)
        }
    }
}
