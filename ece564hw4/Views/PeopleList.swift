//
//  PeopleList.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import SwiftUI

struct PeopleList: View {
    @EnvironmentObject var model: DataModel
    @State private var searchText = ""
    @State private var isEditing = false
    @State private var editingPerson: DukePerson?
    
    var body: some View {
        let peopleList:[DukePerson] = Array(model.database.values)
        var filteredData: [DukePerson] {
            if searchText.isEmpty {
                return peopleList
            } else {
                return peopleList.filter { $0.description.localizedCaseInsensitiveContains(searchText) }
            }
        }
        
        let professors:[DukePerson] = filteredData.filter { $0.role == Role.Professor}
        let tas:[DukePerson] = filteredData.filter { $0.role == Role.TA}
        let students:[DukePerson] = filteredData.filter { $0.role == Role.Student}
        let others:[DukePerson] = filteredData.filter { $0.role == Role.Other}
        
        NavigationSplitView  {
            TextField("Search", text: $searchText)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
           
            List {
                Section(header: Text("Professor")) {
                    ForEach(professors, id: \.self) { dukeperson in
                        NavigationLink(destination: PersonDetail(person: dukeperson)) {
                           PersonRow(person: dukeperson)
                       }
                        .swipeActions {
                            Button("Delete") {
                               _ = sharedModel.delete(dukeperson.DUID)
                               _ = sharedModel.save()
                            }
                            .tint(.red)
                            Button("Edit") {
                                editingPerson = dukeperson
                                isEditing = true
                            }
                            .tint(.gray)
                        }
                    }
                }
                
                Section(header: Text("TA")) {
                    ForEach(tas, id: \.self) { dukeperson in
                        NavigationLink(destination: PersonDetail(person: dukeperson)) {
                           PersonRow(person: dukeperson)
                       }
                        .swipeActions {
                            Button("Delete") {
                                _ = sharedModel.delete(dukeperson.DUID)
                                _ = sharedModel.save()
                            }
                            .tint(.red)
                         
                            Button("Edit") {
                                editingPerson = dukeperson
                                isEditing = true
                            }
                            .tint(.gray)
                           
                        }
                    }
                    
                }
                
                Section(header: Text("Students")) {
                    ForEach(students, id: \.self) { dukeperson in
                        NavigationLink(destination: PersonDetail(person: dukeperson)) {
                           PersonRow(person: dukeperson)
                       }
                        .swipeActions {
                                Button("Delete") {
                                    _ = sharedModel.delete(dukeperson.DUID)
                                    _ = sharedModel.save()
                                }
                                .tint(.red)
                             
                                Button("Edit") {
                                    editingPerson = dukeperson
                                    isEditing = true
                                }
                                .tint(.gray)
                                
                        }
                    }
                    
                }
                
                Section(header: Text("Others")) {
                    ForEach(others, id: \.self) { dukeperson in
                        NavigationLink(destination: PersonDetail(person: dukeperson)) {
                           PersonRow(person: dukeperson)
                       }
                        .swipeActions {
                                Button("Delete") {
                                    _=sharedModel.delete(dukeperson.DUID)
                                    _=sharedModel.save()
                                }
                                .tint(.red)
                             
                                Button("Edit") {
                                    editingPerson = dukeperson
                                    isEditing = true
                                }
                                .tint(.gray)
    
                        }
                    }
                   
                }
            }
            .listStyle(.plain)
            } detail: {
                Text("Select a Person")
            }
//            .sheet(isPresented: $isEditing) {
//                AddEditView(isPresented: $isEditing, person: editingPerson)
//            }
            .sheet(item: $editingPerson) { person in
                AddEditView(isPresented: $isEditing, person: person)
            }
    }
}

//struct PeopleList_Preview: PreviewProvider {
//    static var previews: some View {
//        PeopleList()
//    }
//}




