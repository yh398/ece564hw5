//
//  CircleImage.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    
    var body: some View {
        image
            .resizable()
            .clipShape(Circle())
            .aspectRatio(contentMode: .fit)
            .overlay{
                Circle().stroke(.white, lineWidth:4)
            }
            .shadow(radius: 7)
    }
}

//#Preview {
//    
//    CircleImage(image: Image("ece564_headshot"))
//}
