import SwiftUI

struct ConView: View {
    @State private var isSheetPresented = false
    
    var body: some View {
        VStack {
            Text("Main Content")
            
            Button("Show Modal Sheet") {
                isSheetPresented = true
            }
            .padding()
            .background(Color.blue)
            .foregroundColor(.white)
            .cornerRadius(8)
        }
        .sheet(isPresented: $isSheetPresented) {
            ModalSheetView(isSheetPresented: $isSheetPresented)
        }
    }
}

struct ModalSheetView: View {
    @Binding var isSheetPresented: Bool
    
    var body: some View {
        VStack {
            Text("This is a Modal Sheet")
                .font(.title)
            
            Button("Close") {
                isSheetPresented = false
            }
            .padding()
            .background(Color.blue)
            .foregroundColor(.white)
            .cornerRadius(8)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ConView()
    }
}
