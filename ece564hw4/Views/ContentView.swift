//
//  ContentView.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var model: DataModel
    @State private var isSheetPresented = false
    @State private var isEditing = false
    @State private var selectedOption = ""
    @State private var person: DukePerson = dummyPerson
    
    var body: some View {
        NavigationView {
            VStack{
                HStack{
                    Button("Download Options") {
                        isSheetPresented = true
                    }
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(8)
                    
                    .sheet(isPresented: $isSheetPresented) {
                        VStack(spacing: 20) {
                            Text("Select an Option")
                                .font(.headline)
                            
                            Button(action: {
                                model.downloadReplace()
                                isSheetPresented = false
                            }) {
                                Text("Download and Replace")
                                    .foregroundColor(.blue)
                            }
                            
                            Button(action: {
                                model.downloadUpdate()
                                selectedOption = "Download and Update"
                                isSheetPresented = false
                            }) {
                                Text("Download and Update")
                                    .foregroundColor(.blue)
                            }
                            
                            Button(action: {
                                selectedOption = "Cancel"
                                isSheetPresented = false
                            }) {
                                Text("Cancel")
                                    .foregroundColor(.red)
                            }
                        }
                        .padding()
                        .frame(maxWidth: .infinity, maxHeight: 200)
                        .background(Color.white)
                        .cornerRadius(10)
                        .padding()
                    }
                    NavigationLink(destination: StatView()) {
                        Text("Statistics")
                            .font(.system(size: 20))
                            .padding()
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    Button("+") {
                        isEditing = true
                    }
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(8)
                    .sheet(isPresented: $isEditing) {
                        AddEditView(isPresented: $isEditing, person: dummyPerson)
                    }
                    }
                    
                }
                ProgressView("Download Progress", value: model.downloadProgress)
                    .scaleEffect(0.7)
                
                PeopleList()
            }
            
        }
        .padding()
        .navigationTitle("Cohort - Fall 2023")
        .navigationBarItems(trailing:
                                NavigationLink(destination: Text("New Item View")) {
            Image(systemName: "plus")
        }
        )
        
    }
}

