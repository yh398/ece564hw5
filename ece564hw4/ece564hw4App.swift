//
//  ece564hw4App.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import SwiftUI

@main
struct ece564hw4App: App {
    @StateObject private var model: DataModel = sharedModel
   
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(model)
        }
    }
}
