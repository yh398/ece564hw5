//
//  Utilities.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import Foundation
import SwiftUI


func getDocumentsDirectory() -> URL {
    let paths = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

func stringFromImage(_ imagePic: UIImage) -> String {
    let picImageData: Data = imagePic.jpegData(compressionQuality: 0.2)!
    let picBase64 = picImageData.base64EncodedString()
    return picBase64
}

func imageFromString (base64String: String) -> Image {
    guard let data = Data(base64Encoded: base64String),
          let uiImage = UIImage(data: data) else {
        return Image("ece564_headshot")
    }
    return Image(uiImage: uiImage)
}

func uuidToInt(_ uuid: UUID) -> Int {
    let byteArray = withUnsafeBytes(of: uuid.uuid) { Data($0) }
    let integer = byteArray.withUnsafeBytes { $0.load(as: Int.self) }
    return integer
}

extension String {
    var imageFromBase64: UIImage? {
        guard let imageData = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            return nil
        }
        return UIImage(data: imageData)
    }
}
