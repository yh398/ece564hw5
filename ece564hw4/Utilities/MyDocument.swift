//
//  MyDocument.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import Foundation
import UIKit

class MyDocument: UIDocument {
    var dukeCohort: [DukePerson] = []
    var saveError: Error?
    
    // Called when the file is SAVED, allows for content to be set
    override func contents(forType typeName: String) throws -> Any {
        var json:String? = nil
        if let encoded = try? JSONEncoder().encode(self.dukeCohort) {
            json = String(data:encoded, encoding:.utf8)
            let length = json!.lengthOfBytes(using: String.Encoding.utf8)
            return Data(bytes: json!, count: length)
        } else {
            print("Error SAVING data")
            return Data()
        }
        
    }
    
    // Called when the file is LOADED - sends content of file as "contents"
    override func load(fromContents contents: Any, ofType typeName: String?) throws {
        if let userContent = contents as? Data {
            if let decoded = try? JSONDecoder().decode([DukePerson].self, from: userContent) {
                self.dukeCohort = decoded
            }
        } else {
            print("Problem with loading file")
        }
    }
}
