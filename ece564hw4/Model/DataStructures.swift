//
//  DataStructures.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import Foundation

struct Statistic {
    let category: String
    let value: Double
}

enum Role : String, Codable {
    case Unknown = "Unknown" // has not been specified
    case Professor = "Professor"
    case TA = "TA"
    case Student = "Student"
    case Other = "Other" // has been specified, but is not Professor, TA, or Student
}

enum Gender : String, Codable {
    case Unknown = "Unknown" // has not been specified
    case Male = "Male"
    case Female = "Female"
    case Other = "Other" // has been specified, but is not “Male” or “Female”
}



struct DukePerson: Codable, CustomStringConvertible, Identifiable, Hashable {
    var id: Int{DUID}
    var DUID: Int
    var netid: String
    var fName: String
    var lName: String
    var email: String
    var from: String
    var gender: Gender
    var role: Role
    var program: String
    var plan: String
    var hobby: String
    var languages: [String]
    var movie: String
    var picture: String
    var team: String
    var description: String {
        let pronoun = getPersonalPronoun(for: gender)
        let pronoun2 = getPersonalPronoun2(for: gender)
        let possessive = getPossessive(for: gender)
        if role == Role.Student {
            return "\(fName) \(lName) is a \(role.rawValue.lowercased()). \(pronoun) is from \(from.lowercased()). \(pronoun) is in the \(program) working towards \(plan) degree. \(possessive) hobby is \(hobby) and \(possessive) favorite movie is \(movie). \(possessive) best programming languages are \(languages). You can reach \(pronoun2) at \(email)."
        } else {
            return "\(fName) \(lName) is a \(role.rawValue). \(pronoun) is from \(from). \(possessive) hobby is \(hobby) and \(possessive) favorite movie is \(movie). \(possessive) best programming languages are \(languages). You can reach \(pronoun2) at \(email)."
        }
    }
        
    init(DUID: Int, netid: String, fName: String, lName: String, email: String, from: String, gender: Gender, role: Role, program: String, plan: String, hobby: String, languages: [String], movie: String, picture: String, team: String) {
        self.DUID = DUID
        self.netid = netid
        self.fName = fName
        self.lName = lName
        self.email = email
        self.from = from
        self.gender = gender
        self.role = role
        self.program = program
        self.plan = plan
        self.hobby = hobby
        self.languages = languages
        self.movie = movie
        self.picture = picture
        self.team = team
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(DUID)
    }
    
    enum CodingKeys: String, CodingKey {
            case DUID
            case netid
            case fName
            case lName
            case email
            case from
            case gender
            case role
            case program
            case plan
            case hobby
            case languages
            case movie
            case picture
            case team
        }
    
    private func getPersonalPronoun(for gender: Gender) -> String {
        switch gender {
            case .Male:
                return "He"
            case .Female:
                return "She"
            case .Unknown, .Other:
                return "They"
        }
    }
    
    private func getPersonalPronoun2(for gender: Gender) -> String {
        switch gender {
            case .Male:
                return "him"
            case .Female:
                return "her"
            case .Unknown, .Other:
                return "them"
        }
    }
    
    
    private func getPossessive(for gender: Gender) -> String {
        switch gender {
        case .Male:
            return "His"
        case .Female:
            return "Her"
        case .Unknown, .Other:
            return "Their"
        }
    }
}
