//
//  DataModel.swift
//  ece564hw1
//
//  Created by 何一苇 on 9/9/23.
//

import Foundation
import UIKit
import SwiftUI

class DataModel: NSObject, ObservableObject, URLSessionDataDelegate {
    
    @Published var database: [Int: DukePerson]
    @Published var professor: [DukePerson]
    @Published var ta: [DukePerson]
    @Published var students: [DukePerson]
    @Published var others: [DukePerson]
    @Published var downloadProgress: Float = 0.0
    static let sandboxURL: URL = {
            return getDocumentsDirectory().appendingPathComponent("sandBox.json")
        }()
    static let sandboxDocument: MyDocument = MyDocument(fileURL: sandboxURL)
    var receivedData: Data!
    var totalBytesReceived: Int64!
    var totalBytesExpected: Int64!
    var tempCohort: [Int: DukePerson] = [:]
    @Published var replace = false
    var result: Bool
 
    override init() {
        self.database = [:]
        self.professor = []
        self.ta = []
        self.students = []
        self.others = []
        self.replace = false
        self.result = false
        super.init()
        if filemgr.fileExists(atPath: DataModel.sandboxURL.path) {
            result = load(DataModel.sandboxURL)
        } else {
            result = add(myInfo)
            result = save()
        }
    }
    
    func add(_ newPerson: DukePerson) -> Bool {
        if database[newPerson.DUID] == nil {
            database[newPerson.DUID] = newPerson
            return true
        }
        return false
    }
    
    func update(_ updatedPerson: DukePerson) -> Bool {
        if database[updatedPerson.DUID] != nil {
            database[updatedPerson.DUID] = updatedPerson
            return true
        } else {
            // DUID not found in the database, add updatedPerson
            database[updatedPerson.DUID] = updatedPerson
            return false
        }
    }

    func delete(_ DUID: Int) -> Bool {
        if database[DUID] != nil {
            database[DUID] = nil
            return true
        }
        return false
    }

    func find(_ DUID: Int) -> DukePerson? {
        return database[DUID]
    }

    func find(lastName lName: String, firstName fName: String = "*") -> [DukePerson]? {
        if lName == "*" && fName == "*" {
            return Array(database.values)
        }
        var matchPeople: [DukePerson] = []

        for person in database.values {
            if person.lName.lowercased() == lName.lowercased() && (fName == "*" || person.fName.lowercased() == fName.lowercased()) {
                matchPeople.append(person)
            }
        }
        
       if matchPeople.isEmpty {
           return nil
       } else {
           return matchPeople
       }
    }
    
    func load(_ url:URL) -> Bool {
        var result = false
        if url == sourceURL {
            var tempData: Data = Data()
            do {
                tempData = try Data(contentsOf: url)
            } catch let error as NSError {
                print(error)
                result = false
            }
            if let decoded = try? JSONDecoder().decode([DukePerson].self, from: tempData) {
                for person in decoded {
                    self.database[person.DUID] = person
                }
                result = true
            }
        } else {
            if filemgr.fileExists(atPath: url.path) {
                DataModel.sandboxDocument.open(completionHandler: {(success: Bool) -> Void in
                    if success {
                        print("Load file success from \(url)")
                        for person in DataModel.sandboxDocument.dukeCohort {
                            self.database[person.DUID] = person
                        }
                        result = true
                    } else {
                        result = false
                        print("Failed to load file from \(url)")
                    }
                })
            } else {
                DataModel.sandboxDocument.save(to: DataModel.sandboxURL, for: .forCreating,completionHandler: {(success: Bool) -> Void in
                    if success {
                        print("File \(DataModel.sandboxURL) created OK")
                    }
                    else {
                        print("Failed to create file \(DataModel.sandboxURL)")
                    }
                })

            }
        }
        return result
    }
    
    
    func save() -> Bool {
        var result = false
        DataModel.sandboxDocument.dukeCohort = Array(self.database.values)
        if filemgr.fileExists(atPath: DataModel.sandboxURL.path) {
            DataModel.sandboxDocument.save(to: DataModel.sandboxURL, for: .forOverwriting, completionHandler: {(success: Bool) -> Void in
                if success {
                    print("Successfully save file at \(DataModel.sandboxURL)")
                    result = true
                } else {
                    print("Failed to save file at \(DataModel.sandboxURL)")
                    result = false
                }
            })
        } else {
            DataModel.sandboxDocument.save(to: DataModel.sandboxURL, for: .forCreating, completionHandler: {(success: Bool) -> Void in
                if success {
                    print("Success created file at \(DataModel.sandboxURL)")
                    result = true
                } else {
                    print("Failed to create file at \(DataModel.sandboxURL)")
                    result = false
                }
            })

        }
        
        return result
    }
    
    func upload(website: String, auth: String, update: Bool, completion: @escaping (Bool, Error?) -> Void) -> Bool {
        let endpoint = update ? "/entries/yh398" : "/entries/create"
        let url = URL(string: website + endpoint)
        var request = URLRequest(url: url!)
        request.httpMethod = update ? "PUT" : "POST"
        let authData = auth.data(using: .utf8)!
        let base64Auth = authData.base64EncodedString()
        request.setValue("Basic \(base64Auth)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            let encoder = JSONEncoder()
            if update && !self.database.isEmpty {
                let jsonData = try encoder.encode(self.database[myInfo.DUID])
                request.httpBody = jsonData
            } else {
                let jsonData = try encoder.encode(myInfo)
                request.httpBody = jsonData
            }
        } catch {
            completion(false, error)
            return false
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(false, error)
                return
            } else {
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                    completion(true, nil)
                } else {
                    completion(false, NSError(domain: "Server Error", code: 1, userInfo: nil))
                }
            }
        }
        
        task.resume()
        return true
    }

    func delete(website: String, auth: String, completion: @escaping (Bool, Error?) -> Void) -> Bool {
        let endpoint = "/entries/yh398"
        let url = URL(string: website + endpoint)
        var request = URLRequest(url: url!)
        request.httpMethod = "DELETE"
        let authData = auth.data(using: .utf8)!
        let base64Auth = authData.base64EncodedString()
        request.setValue("Basic \(base64Auth)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(false, error)
                return
            } else {
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                    completion(true, nil)
                } else {
                    completion(false, NSError(domain: "Server Error", code: 1, userInfo: nil))
                }
            }
        }
        task.resume()
        return true
    }

    func download(website: String, auth: String, completion: @escaping (Bool, Error?) -> Void) -> Bool {
        receivedData = Data()
        totalBytesReceived = 0
        totalBytesExpected = -1
        downloadProgress = 0.0
        let endpoint = "/entries/all"
        let url = URL(string: website + endpoint)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        let authData = auth.data(using: .utf8)!
        let base64Auth = authData.base64EncodedString()
        request.setValue("Basic \(base64Auth)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session : URLSession = {
            let config = URLSessionConfiguration.ephemeral
            config.allowsCellularAccess = false
            let session = URLSession(configuration: config, delegate: self, delegateQueue: .main)
            return session
        }()
        let task = session.dataTask(with: request)
        task.resume()
        return true
    }



    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        receivedData.append(data)
        totalBytesReceived += Int64(data.count)
        if totalBytesExpected == -1 {
            if let response = dataTask.response as? HTTPURLResponse {
                totalBytesExpected = Int64(response.expectedContentLength)
            }
        }
        
        if totalBytesExpected > 0 {
            downloadProgress = Float(totalBytesReceived) / Float(totalBytesExpected)
        } else {
            print("Download Progress: Unknown")
        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if let error = error {
            print("Download failed with error: \(error.localizedDescription)")
        } else {
            print("Download success")
            if let decoded = try? JSONDecoder().decode([DukePerson].self, from: receivedData) {
                tempCohort = [:]
                for person in decoded {
                    tempCohort[person.DUID] = person
                }
            }

            if replace {
                self.database = [:]
                for id in tempCohort.keys {
                    self.database[id] = tempCohort[id]
                }
            } else {
                for id in tempCohort.keys {
                    if self.database.keys.contains(id) {
                        self.database[id] = tempCohort[id]
                    } else {
                        continue
                    }
                }
            }
            result = save()
        }
    }
    
    func downloadReplace() {
        self.replace = true
        let auth = "yh398:cce7eaa092a6cda4cf04b92b842098c6b375ab66c6c711026282349dd0a87c32"
        _ = download(website: server, auth: auth) {success, error in
            if success {
                print("Download replace success")
            } else {
                if let error = error {
                    print("Download replaced failed with error: \(error.localizedDescription)")
                } else {
                    print("Download replace failed with an unknown error")
                }
            }
        }
    }
    
    func downloadUpdate() {
        replace = false
        let auth = "yh398:cce7eaa092a6cda4cf04b92b842098c6b375ab66c6c711026282349dd0a87c32"
        _ = download(website: server, auth: auth) {success, error in
            if success {
                print("Download update success")
            } else {
                if let error = error {
                    print("Download update failed with error: \(error.localizedDescription)")
                } else {
                    print("Download update failed with an unknown error")
                }
            }
        }
    }

}


