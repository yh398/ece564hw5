//
//  Constants.swift
//  ece564hw4
//
//  Created by 何一苇 on 10/2/23.
//

import Foundation
import UIKit
import SwiftUI

let sharedModel = DataModel()
let sourceURL = Bundle.main.url(forResource: "ece564class", withExtension: ".json")
let filemgr = FileManager.default

let server = "http://ece564.rc.duke.edu:8080/"
let auth = "yh398:cce7eaa092a6cda4cf04b92b842098c6b375ab66c6c711026282349dd0a87c32"
let myPic = UIImage(named: "ece564_headshot")
let myPicBase64 = stringFromImage(myPic!)
let myInfo = DukePerson(
    DUID: 1283994,
    netid: "yh398",
    fName: "Yiwei",
    lName: "He",
    email: "yiwei.he@duke.edu",
    from:  "Beijing",
    gender: .Male,
    role: .Student,
    program: "Grad - Masters A&S",
    plan: "Computer Science-MS",
    hobby: "Singing",
    languages: ["Python", "Java", "Swift"],
    movie: "Goodfellas",
    picture: myPicBase64,
    team: ""
)

let dummyPic = UIImage(named: "unknown_person")
let dummyPicBase64 = stringFromImage(dummyPic!)

var dummyPerson = DukePerson(
    DUID: 0,
    netid: "",
    fName: "",
    lName: "",
    email: "",
    from:  "",
    gender: .Unknown,
    role: .Unknown,
    program: "",
    plan: "",
    hobby: "",
    languages: [],
    movie: "",
    picture: dummyPicBase64,
    team: ""
)

